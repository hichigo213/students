@if ($student->photos === null )
<div class="container">
    <div>
        <img src="/public/storage/avatar.png" class="img-thumbnail">
    </div>
    <form enctype="multipart/form-data" action="{{ route('photos.store') }}" method="POST">
        <div class="form-group">
          @csrf
          <input type="hidden" name="student_id" value="{{ $student->id }}">
          <input type="file" name="file" id="file" required>
          <button type="submit">Загрузить</button>
        </div>
    </form>
</div>
@else
<div class="container">
  <img src="/public/storage/{{ $student->photos->photo }}" class="img-thumbnail">
    <form enctype="multipart/form-data" action="{{ route('photos.update', $student->photos->id) }}" method="POST">
        <div class="form-group">
          @csrf
          @method('PUT')
          <input type="hidden" name="student_id" value="{{ $student->id }}">
          <input type="file" name="file" id="file" required>
          <button type="submit">Обновить</button>
        </div>
    </form>
</div>
@endif

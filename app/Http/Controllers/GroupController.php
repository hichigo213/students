<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Subject;

use App\Http\Requests\StoreGroup;
use Illuminate\Http\Request;

class GroupController extends Controller
{

    public function index()
    {
        $subjects = Subject::all();
        $groups = Group::all();

        return view('groups.show.index', compact('groups', 'subjects'));
    }

    public function create()
    {
        return view('groups.create');
    }

    public function store(StoreGroup $request)
    {
        Group::create($request->all());

        return redirect('groups');
    }

    public function edit(Group $group)
    {
        return view('groups.edit', compact('group'));
    }

    public function update(StoreGroup $request, Group $group)
    {
        $group->update($request->all());

        return redirect('groups')->with('success', 'Group has been updated');
    }

    public function show(Group $group)
    {
        $subjects = Subject::all();

        return view('groups.show', compact('group', 'subjects'));
    }

    public function destroy(Group $group)
    {
          $group->delete();

          return redirect('groups')->with('success', 'Group has been deleted');
    }
}

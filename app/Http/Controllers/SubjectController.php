<?php

namespace App\Http\Controllers;

use App\Models\Subject;

use App\Http\Requests\StoreSubject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function index()
    {
        $subjects = Subject::all();

        return view('groups.students.edit.index_subject', compact('subjects'));
    }

    public function create()
    {
        return view('groups.students.edit.create_subject');
    }

    public function store(StoreSubject $request, Subject $subject)
    {
        $subject->create($request->all());

        return redirect('subjects')->with('success', 'Subject has been added');
    }

    public function update(Request $request, Subject $subject)
    {
        $subject->update($request->all());

        return back();
    }

    public function destroy(Subject $subject)
    {
          $subject->delete();

          return back();
    }
}

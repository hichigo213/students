<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use App\Models\Student;

use App\Http\Requests\StoreStudent;

class StudentController extends Controller
{
    public function index()
    {
        $subjects = Subject::all();
        $students = Student::with('marks')
                    ->Filter()
                    ->paginate(2)
                    ->appends([
                      'group_id' => request()->group_id,
                      'name' => request()->name
                    ]);

        return view('groups.students.show.index', compact('students', 'subjects'));
    }
    public function create()
    {
        return view('groups.students.create');
    }

    public function store(StoreStudent $request, Student $student)
    {
        $student->create($request->all());

        return back();
    }

    public function show(Student $student)
    {
        $subjects = Subject::all();

        return view('groups.students.show', compact('student', 'subjects'));
    }

    public function edit(Student $student)
    {
        return view('groups.students.edit', compact('student'));
    }

    public function update(StoreStudent $request, Student $student)
    {
        $student->update($request->all());

        return back();
    }

    public function destroy(Student $student)
    {
        $student->delete();

        return back();
    }
}

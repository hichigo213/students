<?php

namespace App\Http\Controllers;

use App\Models\Mark;

use Illuminate\Http\Request;

class MarkController extends Controller
{
    public function store(Request $request, Student $student, Mark $mark)
    {
          $mark->create($request>all());

          return back();
    }

    public function destroy(Mark $mark)
    {
        $mark->delete();

        return back();
    }
}

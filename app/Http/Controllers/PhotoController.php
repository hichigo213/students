<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use App\Models\Student;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class PhotoController extends Controller
{
    public function store(Request $request, Student $student)
    {
        $file = $request->file('file');
        $filename = $request->student_id . '.' . $file->getClientOriginalExtension();
        Storage::disk('public')->putFileAs('', $file, $filename);
        Photo::create(['student_id' => $request->student_id, 'photo' => $filename]);

        return back();
    }

    public function update(Request $request, $id)
    {
        $photo = Photo::find($id);
            $file = $request->file('file');
            $filename = $photo->photo;
            Storage::disk('public')->putFileAs('', $file, $filename);

        return back();
    }
}
